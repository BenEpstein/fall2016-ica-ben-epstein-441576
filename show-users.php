<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Matches</title>
    <link rel='stylesheet' type='text/css' href='style.css'/>
</head>
<body>
<h1>All Users</h1>
<?php
require "db.php";
$stmt = $mysqli->prepare("select name, email, pictureURL, description, age from users order by id");
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->execute();
$stmt->bind_result($name, $email, $image, $description, $age);

echo "<ul>\n";
while ($stmt->fetch()) {
    printf("\t<li>%s<br> %s<br> %s<br> %d<br> <img style='width:300px' src='%s' alt='item picture'/></li>\n",
        htmlspecialchars($name),
        htmlspecialchars($email),
        htmlspecialchars($description),
        htmlspecialchars($age),
        htmlspecialchars($image)
    );
}
echo"</ul>\n";
$stmt->close();
?>

<form enctype="multipart/form-data" method="POST" action="age-range.php">
    <p>Filter by age:</p><br>
    <label for="lowAge">Low age:</label> <br> <input type="number" name="lowAge" id="lowAge"><br>
    <label for="highAge">High Age: </label> <br> <input type="number" name="highAge" id="highAge"><br>
    <input type="submit" name="action" value="Filter">
</form>
</body>

<?php
require 'db.php';
if (isset($_POST['action'])) {
    if ($_POST['action'] == "Profiles") {
        header("Location: show-users.php");
    } elseif (($_POST['action'] == "Submit")) {
        $name = (String)$_POST['name'];
        $age = (int)$_POST['age'];
        $email = (String)$_POST['email'];
        $description = (String)$_POST['description'];
        $image = trim(basename($_FILES['uploadedFile']['name']));
        $full_path = sprintf("/home/BEpstein/public_html/ica/uploads/%s", $image);
        $image_path = sprintf("/~BEpstein/ica/uploads/%s", $image);

        if ((!isset($_POST['name'])) || (!isset($_POST['age'])) || (!isset($_POST['email'])) || (!isset($_POST['description'])) ||
        $image == null)
        {
            echo("Please fill out all forms");
        } else {
            $stmt = $mysqli->prepare("INSERT INTO users(name, email, pictureURL, description, age) values(?,?,?,?,?)");
            if (!$stmt) {
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            $stmt->bind_param('ssssi', $name, $email, $image_path, $description, $age);
            $stmt->execute();
            $stmt->close();


            if (move_uploaded_file($_FILES['uploadedFile']['tmp_name'], $full_path)) {
                echo("Uploaded!");
                header("Location: show-users.php");
                exit;
            } else {
                var_dump("Upload Failed!");
                var_dump($full_path);
                exit;
            }

        }
    }
}
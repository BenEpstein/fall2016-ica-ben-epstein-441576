<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Matchmaking Site - Users by Age</title>
    <link rel='stylesheet' type='text/css' href='style.css'/>
</head>
<body>
<h1>Users in Age Range</h1>
<?php
require "db.php";

if (isset($_POST['action'])) {
    if ($_POST['action'] == "Filter") {
        $low = (int) $_POST['lowAge'] -1; // so it's inclusive
        $high = (int) $_POST['highAge']+1; //so it's inclusive
    }
}


$stmt = $mysqli->prepare("select name, email, pictureURL, description, age from users WHERE age > $low
  AND age < $high;");
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->execute();
$stmt->bind_result($name, $email, $image, $description, $age);

echo "<ul>\n";
while ($stmt->fetch()) {
    printf("\t<li>%s<br> %s<br> %s<br> %d<br> <img style='width:300px' src='%s' alt='item picture'/></li>\n",
        htmlspecialchars($name),
        htmlspecialchars($email),
        htmlspecialchars($description),
        htmlspecialchars($age),
        htmlspecialchars($image)
    );
}
echo"</ul>\n";
$stmt->close();
?>
<form enctype="multipart/form-data" method="POST" action="show-users.php">
    <p>Back to all users:</p><br>
    <input type="submit" name="action" value="All Users">
</form>
</body>
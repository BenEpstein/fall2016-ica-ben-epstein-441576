# Simple online dating website

**In-class assignment to be done alone.**

**Requirements**

1. This must be completed and submitted in 1.5 hours or less for credit.
2. Users can register for accounts and then log in to the website.
3. Accounts should have both a username and a secure salted and hashed password.
4. Users can create a profile with desired ages and gender(s) they are interested in.
5. Users can see all matches on the show-users page.
6. Users can filter the results by age and the page should update automatically.
7. Registered users can edit their profile.
8. All data must be kept in a MySQL database (user information, stories, comments, and links).